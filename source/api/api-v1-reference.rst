API v1 reference
================

API v1 is the current API and thefore is both accessible
at the root of the API microservice as well as at the ``/v1/`` url route.
For future compatibility we recommend using the ``/v1/`` url prefix in case the
API deprecation or changes to a newer version.



Standard format
---------------

In general, requesting, updating or deleting a resource/documentent
can be done with the following HTTP requests:

Getting entity data
^^^^^^^^^^^^^^^^^^^

.. http:get:: /<entity>

  Returns a list of given entity type available to the application.

  **Example request**:

  .. code-block:: http

    GET /<plural-name-of-entity> HTTP/1.1
    Host: api.iwa-atlas.com
    Accept: application/json, text/javascript

  **Example response**:

  .. code-block:: http

    HTTP/1.1 200 OK
    Vary: Accept
    Content-Type: text/javascript

    [{
      "id": <id>,
      "<property>": <value>,
      "<property>": <value>,
      "<subcollection (plural noun)>": [<id>, <id>, <id>]
    },{
      "id": <id>,
      "<property>": <value>,
      "<property>": <value>,
      "<subcollection (plural noun)>": [<id>, <id>, <id>]
    },{
      "id": <id>,
      "<property>": <value>,
      "<property>": <value>,
      "<subcollection (plural noun)>": [<id>, <id>, <id>]
    }]

.. http:get:: /<entity>/<id>

  Returns an object of entity with id provided in URL, if it exists in the database.

  .. code-block:: http

    GET /<plural-name-of-entity>/<id> HTTP/1.1
    Host: api.iwa-atlas.com
    Accept: application/json, text/javascript

  **Example response**:

  .. code-block:: http

    HTTP/1.1 200 OK
    Vary: Accept
    Content-Type: text/javascript

    {
      "id": <id>,
      "<property>": <value>,
      "<property>": <value>,
      "<subcollection (plural noun)>": [<id>, <id>, <id>]
    }


Adding an entity
^^^^^^^^^^^^^^^^

.. http:post:: /<entity>

  Adds a new object of entity type to the database (if application is priviliged to do so).

  .. code-block:: http

    POST /<plural-name-of-entity> HTTP/1.1
    Host: api.iwa-atlas.com
    Accept: application/json, text/javascript
    Content-Type: text/javascript

    {
      "<property>": <value>,
      "<property>": <value>,
      "<property>": <value>
    }

  **Example response**:

  .. code-block:: http

    HTTP/1.1 200 OK
    Vary: Accept
    Content-Type: text/javascript

    {
      "id": <id>,
      "<property>": <value>,
      "<property>": <value>,
      "<subcollection (plural noun)>": [<id>, <id>, <id>]
    }

Updating an entity
^^^^^^^^^^^^^^^^^^

.. http:put:: /<entity>/<id>

  Updates entity with the given id.

  .. code-block:: http

    PUT /<plural-name-of-entity> HTTP/1.1
    Host: api.iwa-atlas.com
    Accept: application/json, text/javascript
    Content-Type: text/javascript

    {
      "id": <id>,
      "<property>": <value>,
      "<property>": <value>,
      "<subcollection (plural noun)>": [<id>, <id>, <id>]
    }

  **Example response**:

  .. code-block:: http

    HTTP/1.1 200 OK
    Vary: Accept
    Content-Type: text/javascript

    {
      "id": <id>,
      "<property>": <value>,
      "<property>": <value>,
      "<subcollection (plural noun)>": [<id>, <id>, <id>]
    }


.. http:patch:: /<entity>/<id>

  Updates a property of an entity with a certain operation (add, subtract, delete, multiply).

  .. code-block:: http

    PATCH /<plural-name-of-entity>/<id> HTTP/1.1
    Host: api.iwa-atlas.com
    Accept: application/json, text/javascript

  **Example response**:

  .. code-block:: http

    HTTP/1.1 200 OK
    Vary: Accept
    Content-Type: text/javascript

    {
      "<property>": <new value>
    }

Deleting an entity
^^^^^^^^^^^^^^^^^^

.. http:delete:: /<entity>/<id>

  Deletes entity with id provided in the URL.

  .. code-block:: http

    DELETE /<plural-name-of-entity>/<id> HTTP/1.1
    Host: api.iwa-atlas.com
    Accept: application/json, text/javascript

  **Example response**:

  .. code-block:: http

    HTTP/1.1 200 OK
    Vary: Accept
    Content-Type: text/javascript

      {
        "id": <id>,
        "<property>": <value>,
        "<property>": <value>,
        "<subcollection (plural noun)>": [<id>, <id>, <id>]
      }
