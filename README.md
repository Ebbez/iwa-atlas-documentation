# IWA Atlas documentation
This folder contains all the source files to build the IWA Atlas documentation with Sphinx. It requires a bit of setup, but most of the work has already been done to ensure an easy setup.

## Prerequisites

### Python
The main software/language that the building software uses is Python. Make sure you have the [latest Python](https://www.python.org/downloads/) installed, and that Python is added to your system path (so you can run the `python` command from CMD/PowerShell).

### Preferably using a venv (probably optional)
In order to isolate the packages for this particular workspace from the rest of the packages on your system, you might want to use venv (virtual environment). You can set up a venv by executing the following commands (in a PowerShell environment):

```
...\docs > python -m venv venv

...\docs > Set-ExecutionPolicy Unrestricted -Scope Process

...\docs > venv\Scripts\activate

...\docs (venv) > 
```
*Note: if using CMD, you can skip `Set-ExecutionPolicy`.*

If you are working in a Linux environment, the setup of a venv looks a bit different:
```
.../docs > python -m venv venv

.../docs > ./venv/bin/activate

.../docs (venv) > 
```

### Sphinx & other required Python packages
For both Windows and Linux, you can execute this command (whether you are in a venv or not):
```
...\docs (venv) > pip install -r requirments.txt
```

### Visual Studio Code
This folder contains a VS Code (Visual Studio Code) workspace file, named `vscode-docs-worksapce.code-workspace`.
You should open that file using the VS Code->`File`->`Open Workspace from File` option.

Install the recommended workspace extensions, and activate them (recommended to only activate them for this workspace!)
The recommended extensions are:
- reStructuredText (by LeXtudio Inc.)
- reStructuredText Syntax hightlighting (by Trond Snekvik)
- Python (by Microsoft)

## How to use
You should now be able to view and edit .rst files with good language & lint support.
Using `Ctrl` + `K` and then pressing `Ctrl` + `R`, you should be able to open a split window with the built .rst file.

### Building documentation
To finalize your build, you can build your documentation using the `make.bat` file. Execute the following command:
```
...\docs (venv) > make html

or

...\docs (venv) > .\make.bat html
```