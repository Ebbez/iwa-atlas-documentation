.. IWA Atlas documentation master file, created by
   sphinx-quickstart on Thu Mar 24 10:27:40 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

IWA Atlas documentation
=======================

.. toctree::
   :maxdepth: 2
   :caption: Table of contents:
   :numbered:

   api/api-v1-reference

IWA Atlas is a weather data collection platform that provides many tools to
retrieve the latest weatherdata. This site contains the documentation
of IWA Atlas.

Both documentation on the map & dashboard (graphical user interface) as well
as the API (development interface) will be available on this site.


IWA Business Dashboard
----------------------
IWA Business Dashboard is a core component of the IWA Atlas package.
Businesses can add or remove developers to use the Business Dashboard as well
as being able to search for raw weather data.

The idea of Business Dashboard is for businesses to get quick access to what
weather stations or data they need without the need to create API requests to
get a view on all accessable data. The Business Dashboard also has a developer
mode which gives developers an quick overview of useful identifying data
(IDs of weather stations, regions, etc).


IWA Atlas Interactive
---------------------
IWA Atlas Interactive offers a UI with an interactive map to see weather data.

IWA API
-------
For IWA to collect and process all data, they have decided to go for an API
infrastructure. This allows IWA to create their own applications (like IWA
Atlas Interactive & IWA Business Dashboard), but it also offers client
businesses of IWA to make their own application to interface with the data
that IWA has to offer.
